import React from "react";
import {StyleSheet, View, Image, Text, SafeAreaView, FlatList, ScrollView, TouchableOpacity} from "react-native";
import Feather from "react-native-vector-icons/Feather";
import CATEGORIES_DATA from "../assets/data/categoriesData";
import POPULAR_DATA from "../assets/data/popularData";
import COLORS from "../assets/colors/colors";
import CategoryItem from "./CategoryItem";
import PopularItem from "./PopularItem";


const Home = ({ navigation }) => {


    return (
        <View style={styles.container}>
            {/* Header */}
            <SafeAreaView>
                <View style={styles.headerWrapper}>
                    <Image
                        style={styles.profileImage}
                        source={require('../assets/images/profile.png')}
                    />
                    <Feather
                        name='menu'
                        size={24}
                        color={COLORS.textDark}
                    />
                </View>
            </SafeAreaView>

            {/* Titles */}
            <View style={styles.titlesWrapper}>
                <Text style={styles.titlesSubtitle}>Food</Text>
                <Text style={styles.titlesTitle}>Delivery</Text>
            </View>

            {/* Search */}
            <View style={styles.searchWrapper}>
                <Feather name='search' size={16} color={COLORS.textDark} />
                <View style={styles.search}>
                    <Text style={styles.searchText}>Search...</Text>
                </View>
            </View>

            <ScrollView
                contentInsetAdjustmentBehavior='automatic'
                showsVerticalScrollIndicator={false}
            >
                {/* Categories */}
                <View style={styles.categoriesWrapper}>
                    <Text style={styles.categoriesTitle}>Categories</Text>
                    <View style={styles.categoriesListWrapper}>
                        <FlatList
                            data={CATEGORIES_DATA}
                            renderItem={CategoryItem}
                            keyExtractor={(item) => item.id}
                            horizontal={true}
                            showsHorizontalScrollIndicator={false}
                        />
                    </View>
                </View>

                {/* Popular */}
                <View style={styles.popularWrapper}>
                    <Text style={styles.popularTitle}>Popular</Text>
                    {
                        POPULAR_DATA.map((item) => (
                            <TouchableOpacity
                                style={styles.shadowButton}
                                key={item.id}
                                onPress={() => navigation.navigate('Details', {item: item})}
                            >
                                <PopularItem data={item} />
                            </TouchableOpacity>
                        ))
                    }
                </View>

            </ScrollView>

        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    headerWrapper: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      paddingHorizontal: 20,
      paddingTop: 40,
      alignItems: 'center'
    },
    profileImage: {
        width: 40,
        height: 40,
        borderRadius: 40
    },
    titlesWrapper: {
        marginTop: 30,
        paddingHorizontal: 20
    },
    titlesSubtitle: {
        fontSize: 16,
        color: COLORS.textDark
    },
    titlesTitle: {
        fontSize: 32,
        fontWeight: 'bold',
        color: COLORS.textDark,
        marginTop: 5
    },
    searchWrapper: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: 20,
        marginTop: 30,
        marginBottom: 15
    },
    search: {
        flex: 1,
        marginLeft: 10,
        borderBottomColor: COLORS.textLight,
        borderBottomWidth: 1
    },
    searchText: {
        fontWeight: '500',
        fontSize: 14,
        marginBottom: 5,
        color: COLORS.textLight
    },
    categoriesWrapper: {
      marginTop: 15
    },
    categoriesTitle: {
        fontWeight: 'bold',
        fontSize: 16,
        paddingHorizontal: 20
    },
    categoriesListWrapper: {
        paddingTop: 10,
        paddingBottom: 20,
    },
    popularWrapper: {
        paddingHorizontal: 20,
    },
    popularTitle: {
        fontWeight: 'bold',
        fontSize: 16,
    },
    shadowButton: {
        marginVertical: 10,
        borderRadius: 25,
        shadowColor: '#000',
        shadowRadius: 25,
        elevation: 5
    },
});

export default Home;