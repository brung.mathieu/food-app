import React from "react";
import { StyleSheet, View, Text, Image } from "react-native";
import COLORS from "../assets/colors/colors";

const IngredientItem = (props) => {

    const item = props.item;

    return (
        <View style={[styles.ingredientWrapper, {
            marginLeft: item.id == 1 ? 20 : 0
        }]}>
            <Image source={item.image} style={styles.ingredientsImage} />
        </View>
    )
}

const styles = StyleSheet.create({
    ingredientWrapper: {
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center',
        paddingHorizontal: 10,
        marginRight: 10,
        marginVertical: 5,
        borderRadius: 15,
        shadowColor: COLORS.textDark,
        shadowRadius: 15,
        elevation: 3
    },
    ingredientsImage: {
        resizeMode: 'contain'
    },
})

export default IngredientItem;