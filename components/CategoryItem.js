import React from "react";
import { StyleSheet, View, Image, Text, TouchableOpacity } from "react-native";
import Feather from "react-native-vector-icons/Feather";
import COLORS from "../assets/colors/colors";
import {Shadow} from "react-native-shadow-2";

const CategoryItem = (props) => {
    const item = props.item;

    return (
        <View
            style={[styles.itemWrapper, {
                backgroundColor: item.selected ? COLORS.primary : 'white',
                marginLeft: item.selected ? 20 : 10,
            }]}
        >
            <Shadow distance={4} radius={20} startColor='#E7E7E7' >
                <View style={styles.shadowBox}>

                    <Image style={styles.image} source={item.image} />
                    <Text style={styles.title} >{ item.title }</Text>

                    <TouchableOpacity>
                        <View
                            style={[styles.iconWrapper, {
                                backgroundColor: item.selected ? 'white' : COLORS.secondary
                            }]}
                        >

                            <Feather
                                style={[styles.icon, {
                                    color: item.selected ? COLORS.textDark : 'white',
                                }]}
                                name='chevron-right' size={18}
                            />

                        </View>
                    </TouchableOpacity>
                </View>
            </Shadow>
        </View>
    )
}

const styles = StyleSheet.create({
    itemWrapper: {
        alignItems: 'center',
        marginRight: 10,
        marginVertical: 10,
        borderRadius: 20,
        shadowColor: '#000',
        shadowRadius: 20,
        elevation: 2
    },
    shadowBox: {
        alignItems: 'center',
    },
    image: {
        width: 60,
        height: 60,
        marginTop: 25,
        marginHorizontal: 20
    },
    title: {
        fontSize: 14,
        fontWeight: '500',
        marginTop: 20,
        color: COLORS.textDark
    },
    iconWrapper: {
        alignSelf: 'center',
        justifyContent: 'center',
        marginTop: 20,
        width: 32,
        height: 32,
        borderRadius: 16,
        marginBottom: 20,
    },
    icon: {

        alignSelf: 'center'
    }
});

export default CategoryItem;