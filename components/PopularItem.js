import React from "react";
import { StyleSheet, View, Image, Text  } from "react-native";
import COLORS from "../assets/colors/colors";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import Feather from "react-native-vector-icons/Feather";

const PopularItem = (props) => {
    const item = props.data;

    return (
        <View style={styles.cardWrapper} >
            <View>

                <View style={styles.topWrapper}>
                    <MaterialCommunityIcons name='crown' size={18} color={COLORS.primary} />
                    <Text style={styles.topText}>Top of the week</Text>
                </View>

                <View style={styles.titlesWrapper}>
                    <Text style={styles.titlesTitle}>{ item.title }</Text>
                    <Text style={styles.titleWeight}>Weight { item.weight }</Text>
                </View>

                <View style={styles.cardBottom}>
                    <View style={styles.addButton}>
                        <Feather name='plus' size={20} color={COLORS.textDark} />
                    </View>

                    <View style={styles.ratingWrapper}>
                        <MaterialCommunityIcons name='star' size={14} color={COLORS.textDark} />
                        <Text style={styles.rating}>{ item.rating }</Text>
                    </View>

                </View>

            </View>

            <View style={styles.cardImageWrapper} >
                <Image style={styles.cardImage} source={item.image} />
            </View>

        </View>
    )
}

const styles = StyleSheet.create({
    cardWrapper: {
        flexDirection: 'row',
        backgroundColor: 'white',
        borderRadius: 25,
        paddingTop: 20,
        paddingLeft: 20,
        overflow: "hidden",
    },
    topWrapper: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    topText: {
        marginLeft: 10,
        fontWeight: '500',
        fontSize: 14
    },
    titlesWrapper: {
        marginTop: 20,
    },
    titlesTitle: {
        fontWeight: '500',
        fontSize: 14,
        color: COLORS.textLight
    },
    titleWeight: {
        fontSize: 12,
        color: COLORS.textLight,
        marginTop: 5
    },
    cardBottom: {
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 10,
        marginLeft: -20
    },
    addButton: {
        backgroundColor: COLORS.primary,
        paddingHorizontal: 30,
        paddingVertical: 12,
        borderBottomLeftRadius: 25,
        borderTopRightRadius: 25
    },
    ratingWrapper: {
        flexDirection: 'row',
        alignItems: 'center',
        marginLeft: 20
    },
    rating: {
        fontSize: 12,
        color: COLORS.textDark,
        marginLeft: 5
    },
    cardImageWrapper: {
        marginLeft: 10
    },
    cardImage: {
        width: 210,
        height: 125,
        resizeMode: 'contain'
    },
});

export default PopularItem;