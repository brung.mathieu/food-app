import React from "react";
import {StyleSheet, SafeAreaView, ScrollView, View, Text, TouchableOpacity, Image, FlatList} from "react-native";
import Feather from "react-native-vector-icons/Feather";
import COLORS from "../assets/colors/colors";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import IngredientItem from "./IngredientItem";

const Details = ({ route, navigation }) => {

    const { item } = route.params;

    return (
        <View style={styles.container} >
            {/* Header */}
            <SafeAreaView>
                <View style={styles.headerWrapper}>
                    <TouchableOpacity onPress={() => navigation.goBack()}>
                        <View style={styles.headerLeft}>
                            <Feather name='chevron-left' size={18} color={COLORS.textDark} />
                        </View>
                    </TouchableOpacity>

                    <TouchableOpacity>
                        <View style={styles.headerRight}>
                            <MaterialCommunityIcons name='star' size={18} color='#fff' />
                        </View>
                    </TouchableOpacity>

                </View>
            </SafeAreaView>

            {/* Body */}
            <ScrollView>
                {/* Titles */}
                <View style={styles.titlesWrapper}>
                    <Text style={styles.title}>{ item.title }</Text>
                </View>

                {/* Price */}
                <View style={styles.priceWrapper}>
                    <Text style={styles.priceText}>${item.price}</Text>
                </View>

                {/* Pizza info */}
                <View style={styles.infoWrapper}>
                    <View style={styles.infoLeftWrapper}>

                        <View style={styles.infoItemWrapper}>
                            <Text style={styles.infoItemTitle}>Size</Text>
                            <Text style={styles.infoItemText}>{ item.sizeName } { item.sizeNumber }"</Text>
                        </View>

                        <View style={styles.infoItemWrapper}>
                            <Text style={styles.infoItemTitle}>Crust</Text>
                            <Text style={styles.infoItemText}>{ item.crust }</Text>
                        </View>

                        <View style={styles.infoItemWrapper}>
                            <Text style={styles.infoItemTitle}>Delivery in</Text>
                            <Text style={styles.infoItemText}>{ item.deliveryTime } min</Text>
                        </View>

                    </View>

                    <View>
                        <Image source={item.image} style={styles.itemImage} />
                    </View>

                </View>

                {/* Ingredients */}
                <View style={styles.ingredientsWrapper}>
                    <Text style={styles.ingredientsTitle}>Ingredients</Text>
                    <View style={styles.ingredientsListWrapper}>
                        <FlatList
                            data={item.ingredients}
                            renderItem={IngredientItem}
                            keyExtractor={(item) => item.id}
                            horizontal={true}
                            showsHorizontalScrollIndicator={false}
                        />
                    </View>
                </View>

                {/* Button */}
                <TouchableOpacity onPress={() => alert('Your order aas been placed !')}>
                    <View style={styles.orderWrapper}>
                        <Text style={styles.orderText}>Place an order</Text>
                        <Feather name='chevron-right' size={18} color={COLORS.textDark} />
                    </View>
                </TouchableOpacity>

            </ScrollView>

        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    headerWrapper: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: 20,
        paddingTop: 40,
        marginBottom: 10
    },
    headerLeft: {
        borderColor: COLORS.textLight,
        borderRadius: 10,
        borderWidth: 2,
        padding: 12,
    },
    headerRight: {
        backgroundColor: COLORS.primary,
        borderColor: COLORS.primary,
        borderRadius: 10,
        borderWidth: 2,
        padding: 12,
    },
    titlesWrapper: {
        paddingHorizontal: 20,
        marginTop: 10
    },
    title: {
        color: COLORS.textDark,
        fontSize: 32,
        fontWeight: 'bold',
        width: '50%'
    },
    priceWrapper: {
        marginTop: 20,
        paddingHorizontal: 20
    },
    priceText: {
        color: COLORS.price,
        fontWeight: 'bold',
        fontSize: 32
    },
    infoWrapper: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginTop: 40
    },
    infoLeftWrapper: {
        paddingLeft: 20
    },
    infoItemWrapper: {
        marginBottom: 20
    },
    infoItemTitle: {
        color: COLORS.textLight,
        fontSize: 14,
    },
    infoItemText: {
        color: COLORS.textDark,
        fontSize: 18,
        fontWeight: '700'
    },
    itemImage: {
        resizeMode: 'contain',
        marginLeft: 30
    },
    ingredientsWrapper: {
        marginTop: 30
    },
    ingredientsTitle: {
        color: COLORS.textDark,
        paddingHorizontal: 20,
        fontSize: 16,
        fontWeight: 'bold'
    },
    ingredientsListWrapper: {
        paddingVertical: 10
    },
    orderWrapper: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: COLORS.primary,
        marginVertical: 30,
        marginHorizontal: 20,
        borderRadius: 50,
        paddingVertical: 20
    },
    orderText: {
        fontSize: 18,
        fontWeight: 'bold',
        marginRight: 10
    },
})

export default Details;