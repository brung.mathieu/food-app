const CATEGORIES_DATA = [
    {
        id: '1',
        image: require('../images/wrap-icon.png'),
        title: 'Wrap chicken',
        selected: true
    },
    {
        id: '2',
        image: require('../images/pizza-icon.png'),
        title: 'Pizza',
        selected: false
    },
    {
        id: '3',
        image: require('../images/fried-chicken-icon.png'),
        title: 'Chicken wings',
        selected: false
    },
    {
        id: '4',
        image: require('../images/shrimp-icon.png'),
        title: 'Seafood',
        selected: false
    },
    {
        id: '5',
        image: require('../images/soda-icon.png'),
        title: 'Soft drinks',
        selected: false
    }
]

export default CATEGORIES_DATA;